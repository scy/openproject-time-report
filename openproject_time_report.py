#!/usr/bin/env python3

from argparse import ArgumentParser
from datetime import date, timedelta
import json
from os import environ
import re
from typing import Any, Dict, Iterable, List, Tuple, Union
from urllib.parse import urljoin

import requests
from requests.auth import HTTPBasicAuth


APIKEY_VAR = "OPENPROJECT_APIKEY"
API_BASEPATH = "/api/v3"


def parse_date(date_str: str) -> date:
    split = date_str.split("-")
    if len(split) != 3:
        raise ValueError(f"invalid date: {date_str}")
    return date(*map(int, split))


def parse_duration(duration: str) -> timedelta:
    m = re.match(r"^PT(?:(\d+)H)?(?:(\d+)M)?(?:(\d+(?:\.\d+))S)?$", duration)
    if not m:
        raise ValueError(f"invalid duration: {duration}")
    return timedelta(
        hours=int(m[1]) if m[1] else 0,
        minutes=int(m[2]) if m[2] else 0,
        seconds=int(m[3]) if m[3] else 0,
    )


class FormatError(Exception):
    pass


class Project:
    by_href: Dict[str, "Project"] = {}

    def __init__(self, data: Dict[str, Any]):
        if data.get("_type", None) != "Project":
            raise FormatError("not a project")
        self.id: int = data["id"]
        self.identifier: str = data["identifier"]
        self.name: str = data["name"]
        self.href: str = data["_links"]["self"]["href"]
        self.by_href[self.href] = self
        self.parent_of: List["Project"] = []
        self.parent: Union[str, "Project"] = data["_links"] \
            .get("parent", {"href": None}).get("href")
        # Dereference a href to a Project object.
        parent_proj = self.by_href.get(self.parent, None)
        if parent_proj is not None:
            self.parent = parent_proj
            parent_proj.parent_of.append(self)
        # Use us as the parent for every project that's referring to us.
        for proj in self.by_href.values():
            if proj.parent == self.href:
                proj.parent = self
                self.parent_of.append(proj)

    def __str__(self) -> str:
        return f"Project {self.id} ({self.identifier}): {self.name}"

    @property
    def is_leaf(self) -> bool:
        return len(self.parent_of) == 0


class WorkPackage:
    def __init__(self, data: Dict[str, str]):
        self.href: str = data["href"]
        self.title: str = data["title"]
        self.id: int = int(self.href.split("/")[-1])

    def __eq__(self, other: "WorkPackage") -> bool:
        return self.id == other.id

    def __hash__(self) -> int:
        return self.id

    def __str__(self) -> str:
        return f"#{self.id}: {self.title}"


class TimeEntry:
    def __init__(self, data: Dict[str, Any]):
        if data.get("_type", None) != "TimeEntry":
            raise FormatError("not a time entry")
        self.id: int = data["id"]
        self.comment: str = data["comment"]["raw"]
        self.spent_on: date = parse_date(data["spentOn"])
        self.project: Union[str, Project] = data["_links"]["project"]["href"]
        if self.project in Project.by_href:
            self.project = Project.by_href[self.project]
        self.workpackage = WorkPackage(data["_links"]["workPackage"])
        self.duration: timedelta = parse_duration(data["hours"])
        self.hours: float = self.duration.total_seconds() / 3600

    def __str__(self) -> str:
        return f"{self.comment} ({round(self.hours, 2)}h)"


class EntryAggregate:
    def __init__(
        self,
        over: Union[dict, Iterable[TimeEntry], Iterable["EntryAggregate"]],
        name: str = None,
    ):
        self.name = name
        if isinstance(over, dict):
            over = [
                EntryAggregate(over=v, name=k)
                for k, v in over.items()
            ]
        self.entries = over
        self.hours = sum(entry.hours for entry in self.entries)

    def __len__(self):
        return len(self.entries)

    def __iter__(self):
        return iter(self.entries)

    def __str__(self):
        entrylines = "\n".join(str(entry) for entry in self).splitlines()
        return ("" if self.name is None else f"{self.name}: ") \
            + f"{self.hours} hours\n" \
            + "\n".join(f"  {line}" for line in entrylines)


class Client:
    def __init__(self, endpoint: str, apikey: str = None):
        if apikey is None:
            if APIKEY_VAR not in environ:
                raise KeyError("no API key provided")
            apikey = environ[APIKEY_VAR]
        endpoint = endpoint.rstrip("/")
        if not endpoint.endswith(API_BASEPATH):
            endpoint = endpoint + API_BASEPATH
        self.endpoint = endpoint + "/"
        self.apikey = apikey
        self.session = requests.Session()
        self.session.auth = HTTPBasicAuth("apikey", apikey)
        self._projects = None

    def request(
        self, method: str, path: str, **kwargs
    ) -> Tuple[requests.Response, Dict[str, Any]]:
        res = self.session.request(
            method,
            urljoin(self.endpoint, path),
            **kwargs,
        )
        res.raise_for_status()
        return res, res.json()

    def iter_collection(
        self, path: str, page_size: int = 100, **kwargs
    ) -> Iterable[dict]:
        kwargs = dict(kwargs)
        params = kwargs.get("params", {})
        params["pageSize"] = page_size
        kwargs["params"] = params
        while True:
            _, data = self.request("GET", path, **kwargs)
            if not data.get("_type", None) == "Collection":
                raise FormatError("no collection was returned")
            els = data.get("_embedded", {"elements":[]}).get("elements", [])
            for el in els:
                yield el
            next_page = data.get("_links", {}).get("nextByOffset", None)
            if next_page is None or "href" not in next_page:
                break
            path = next_page["href"]

    def get_projects(self, refresh: bool = False) -> List[Project]:
        if refresh or self._projects is None:
            self._projects = [
                Project(data)
                for data in self.iter_collection("projects")
            ]
        return self._projects

    def get_time_entries(
        self,
        project: Project,
        min_date: date = None,
        max_date: date = None,
    ) -> Iterable[TimeEntry]:
        filters = [{
            "project": {
                "operator": "=",
                "values": [project.id],
            },
        }]
        if min_date or max_date:
            min_date = date(1900, 1, 1) if not min_date else min_date
            max_date = date(2100, 1, 1) if not max_date else max_date
            filters.append({"spent_on": {
                "operator": "<>d", "values": [
                    min_date.isoformat(), max_date.isoformat()
                ]
            }})
        for data in self.iter_collection("time_entries", params={
            "filters": json.dumps(filters),
        }):
            yield TimeEntry(data)

    def by_workpackage(self, begin: date = None, end: date = None):
        collect: Dict[Project, Dict[WorkPackage, Dict[date, List[TimeEntry]]]] = {}
        for project in [p for p in self.get_projects() if p.is_leaf]:
            collect[project] = {}
            for entry in self.get_time_entries(project, begin, end):
                if entry.workpackage not in collect[project]:
                    collect[project][entry.workpackage] = {}
                if entry.spent_on not in collect[project][entry.workpackage]:
                    collect[project][entry.workpackage][entry.spent_on] = []
                collect[project][entry.workpackage][entry.spent_on] \
                    .append(entry)
        return EntryAggregate(collect)



if __name__ == "__main__":
    parser = ArgumentParser(
        description="Fetch time tracking entries from OpenProject",
    )
    parser.add_argument(
        "-u, --url",
        required=True,
        dest="url",
        help="URL of the OpenAPI installation",
    )
    parser.add_argument(
        "-b, --begin-date",
        dest="begin_date",
        type=parse_date,
        metavar="DATE",
        help="only consider entries on this day or later",
    )
    args = parser.parse_args()
    c = Client(args.url)
    result = c.by_workpackage(args.begin_date)
    print(result)
